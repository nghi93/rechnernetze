package aufgabe3.server;

import java.util.HashSet;
import java.util.Set;
import aufgabe3.util.User;

/** 
 * @author Khang Nghi Lam, Heinrich Latreider
 */

public class ChatServer{
	private DispatcherThread dispatcherThread;
	private Set<User> users;
	private final int maxNoOfClients = 100;
	
	public ChatServer(int port){
		dispatcherThread = new DispatcherThread(port, maxNoOfClients, this);
		Thread dispatcher = new Thread(dispatcherThread);
		dispatcher.start();	
		users = new HashSet<User>();
	}	
	
	public static void main(String[] args) {
		int port = 50000;
		new ChatServer(port);
	}	
	
	public boolean newUser(User name){
		return this.users.add(name);
	}
	
	public void signOff(User name){
		this.users.remove(name);
	}
	
	public Set<User> getUser(){
		return new HashSet<User>(users);
	}
}