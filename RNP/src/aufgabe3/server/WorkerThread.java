package aufgabe3.server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.util.Set;
import java.util.logging.Logger;

import aufgabe3.util.User;
import aufgabe3.util.UserImpl;

public class WorkerThread implements Runnable{
	private Socket socket;
	private ChatServer server;
	private final int MAX_SIZE = 255;
	private static final Logger log = Logger.getLogger( WorkerThread.class.getName() );
	private DispatcherThread dispatcher;
	private InputStreamReader reader;
	private User user;
	
	public WorkerThread(Socket socket, ChatServer server, DispatcherThread dispatcher){
		this.socket = socket;
		this.server = server;
		this.dispatcher = dispatcher;
		this.user = null;
		try {
			this.reader = new InputStreamReader(socket.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try{		
			while(true){
				String read = read();
				String command = getCommand(read);
				String getArgs = getArgs(read); 	//kann auch null sein				
				String ret;
				
				// max. 2 Parameter ohne Sonderzeichen?
				if(!read.matches("[A-Z]+(\\s[a-zA-Z0-9]*)?")){
					ret = "ERROR unknown/invalid command";
				}
				else{					
					if(command.equals("NEW") && user == null){
						// Benutzername max 20 Zeichen lang?
						if(getArgs.matches("[a-zA-Z0-9]{1,20}")){
							User user = new UserImpl(getArgs, socket.getInetAddress().getHostAddress(), socket.getPort());
							ret = addUserToUserList(user);
						}
						else{
							ret = "ERROR Benutzername darf max. 20 Zeichen lang sein und keine Sonderzeichen enthalten";
						}
					}
					else if(command.equals("BYE") && user != null){
						bye();
						return;
					}
					else if(command.equals("INFO") && user != null){											
						ret = getUserList();
					}
					else{
						ret = "ERROR UNKOWN/INVALID COMMAND";
					}
				}
				write(ret);		
				
				// Verbindung vom Client trennen, wenn ERROR auftritt
				if(ret.startsWith("ERROR")){
					bye();
					return;
				}
			}
		} catch (SocketException e){
			bye();
		} catch (IOException e) {
			log.severe(e.toString());
		}
	}
	
	public String addUserToUserList(User u){
		String ret;
		if(server.newUser(u)){
			this.user = u;
			ret = "OK";
		}
		else{
			ret = "ERROR Benutzer existiert bereits";
		}
		return ret;
	}
	
	public String getUserList(){
		Set<User> set = server.getUser();
		StringBuilder sb = new StringBuilder();
		sb.append("LIST " + set.size());
		for(User u : set){
			sb.append(" " + u.getHost().toString() + " " + u.getName());
		}	
		return sb.toString();
	}
	
	public String read() throws IOException{
		StringBuilder msg = new StringBuilder();
		
		for(int i = 0; i < MAX_SIZE; i++){
			int a = reader.read();			
			if(a == -1){
				return "ERROR";
			}
			
			char c = (char) a;
			if(c == '\n'){
				log.info("Msg erhalten 1: " + msg);
				return new String(msg);
			}
			msg.append(c); 
		}
		log.info("Msg erhalten 2: " + msg);
		return new String(msg);
	}
	
	public void write(String msg) throws IOException{
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8"));
		log.info("Msg senden: " + msg + "\n");
		printWriter.print(msg + "\n");
		printWriter.flush();
	}
	
	public String getCommand(String s){
		String res[] = s.split(" ");
		return res[0];
	}
	
	public String getArgs(String s){
		String res[] = s.split(" ");
		
		if(res.length > 1){
			return res[1];
		}
		else{
			return null;
		}
	}
	
	private void bye(){
		try {
			if(this.user != null){
				server.signOff(this.user);
			}
			write("BYE");
			socket.close();
			dispatcher.close();			
		} catch (IOException e) {
			log.severe(e.toString());
		}
	}
}
