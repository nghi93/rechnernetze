package aufgabe3.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;
import java.util.logging.Logger;

/**
 * Weist eingehende Anfragen an WorkerThread weiter.
 * 
 * @author khangnghi, Heinrich
 *
 */
public class DispatcherThread implements Runnable {
	private ServerSocket serverSocket;
	private boolean accept = true;
	private int numberOfThreads;
	private ChatServer server;
	private Semaphore sema;
	private static final Logger log = Logger.getLogger( DispatcherThread.class.getName() );
	private int maxNumberOfThreads;
	private int port;
	
	public DispatcherThread(int port, int maxNumberOfThreads, ChatServer server){
		try {
			serverSocket = new ServerSocket(port);
			sema = new Semaphore(maxNumberOfThreads);
			this.numberOfThreads = 0;
			this.server = server;
			this.maxNumberOfThreads = maxNumberOfThreads;
			this.port = port;
		}
		catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	@Override
	public void run() {
		try {		
			Socket socket;
			while(accept){
				if(numberOfThreads == maxNumberOfThreads){
					serverSocket.close();
				}
				sema.acquire();		
				
				if(serverSocket.isClosed()){
					serverSocket = new ServerSocket(port);
				}
				socket = serverSocket.accept();
				numberOfThreads++;
				log.info("Anfrage bekommen von " + socket.getInetAddress() + ":"+ socket.getPort());
				WorkerThread worker = new WorkerThread(socket, server, this);
				Thread t1 = new Thread(worker, "Worker");
				t1.start();	
			}
		} 	
		catch (IOException e) {
			log.severe(e.toString());
		}
		catch(InterruptedException e){
			log.severe(e.toString());
		}
	}
	
	synchronized void close(){
		sema.release();
		numberOfThreads--;
	}
}
