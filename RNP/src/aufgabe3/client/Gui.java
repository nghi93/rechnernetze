package aufgabe3.client;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import aufgabe3.util.User;

public class Gui extends JFrame {

	private static final long serialVersionUID = 1L;
	private final String newline = "\n";
	Dimension screenSize;
	ChatClient client;
	JTextField username;
	JTextField host;
	private boolean loggedIn = false;
	JPanel login = new JPanel();
	JLabel label = new JLabel();
	Container pane;
	JButton button;
	JScrollPane scroll1;
	JScrollPane scroll2;
	JTextField message;
	JTextArea protocol;
	JTextArea userlist;
	String name;
	JButton endButton;
	JButton sendButton;
	
	
	
	public Gui(ChatClient client) {
		super("Messenger");
		this.client = client;
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setResizable(false);
		setLocation(new Point(screenSize.width-500,0));	
		addWindowListener(new MyWindoListener());
		
		pane = this.getContentPane();
		pane.setPreferredSize(new Dimension(500, screenSize.height - 100));

	    pane.setLayout(new GridBagLayout());
	    GridBagConstraints c = new GridBagConstraints();

	    label = new JLabel();
	    label.setPreferredSize(new Dimension(150, 25));
	    label.setMinimumSize(new Dimension(150, 25));
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 0;
	    c.gridx = 0;
	    c.gridy = 0;
	    c.insets = new Insets(0, 100, 0, 100);
	    pane.add(label, c);
	    
	    /**
	     * Login-Dialog
	     */
	    username = new JTextField("Benutzername");
	    username.setPreferredSize(new Dimension(150, 25));
	    username.setMinimumSize(new Dimension(150, 25));
	    username.addFocusListener(new Clear());
	    
	    c.weightx = 0;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.gridx = 0;
	    c.gridy = 1;
	    pane.add(username, c);

	    host = new JTextField("Server");
	    host.setPreferredSize(new Dimension(150, 25));
	    host.setMinimumSize(new Dimension(150, 25));
	    host.addFocusListener(new Clear2());
	    host.addKeyListener(new KeyListener(){

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					button.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
	    	
	    });
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 0.5;
	    c.gridx = 0;
	    c.gridy = 2;
	    pane.add(host, c);	    
	    
	    button = new JButton("Login");
	    button.setPreferredSize(new Dimension(150, 30));
	    button.setMinimumSize(new Dimension(150, 30));
	    button.addKeyListener(new KeyListener(){

			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				if(e.getKeyCode() == KeyEvent.VK_ENTER){
					button.doClick();
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
	    	
	    });
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 0.5;
	    c.gridx = 0;
	    c.gridy = 3;
	    button.addActionListener(new ClickListener());
	    pane.add(button, c);
	    
		pack();		
	}

	private class Clear implements FocusListener{
		@Override
		public void focusGained(FocusEvent e) {
			if(username.getText().equals("Benutzername")){
				username.setText("");
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			if(username.getText().equals("")){
				username.setText("Benutzername");
			}
		}
		
	}
	
	private class Clear2 implements FocusListener{
		@Override
		public void focusGained(FocusEvent e) {
			if(host.getText().equals("Server")){
				host.setText("");
			}
		}

		@Override
		public void focusLost(FocusEvent e) {
			if(host.getText().equals("")){
				host.setText("Server");
			}
		}
		
	}	
	
	public void createChatGUI(Container pane){
		pane.setPreferredSize(new Dimension(500, screenSize.height - 100));

	    pane.setLayout(new GridBagLayout());
	    
	    GridBagConstraints c = new GridBagConstraints();

	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 0;
	    c.gridx = 0;
	    c.gridy = 0;
	    
		userlist = new JTextArea();
		userlist.setEditable(false);
		//userlist.setPreferredSize(new Dimension(100, 320));
		userlist.setMinimumSize(new Dimension(300, 200));
		scroll1 = new JScrollPane(userlist);
		scroll1.setPreferredSize(new Dimension(100, 320));
		pane.add(scroll1, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 0;
	    c.gridx = 1;
	    c.gridy = 0;
		
		protocol = new JTextArea();
		protocol.setEditable(false);
		protocol.setMinimumSize(new Dimension(300, 320));
		//protocol.setPreferredSize(new Dimension(300, 320));
		scroll2 = new JScrollPane(protocol, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll2.setPreferredSize(new Dimension(300, 320));
		pane.add(scroll2, c);
		
		message = new JTextField();
		message.setPreferredSize(new Dimension(300, 25));
		message.addKeyListener(new Key());
		message.setDocument(new PlainDocument(){
			private static final long serialVersionUID = 1L;
			int limit = 100;
			@Override
			public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {
			    if (str == null)
			      return;

			    if ((getLength() + str.length()) <= limit) {
			      super.insertString(offset, str, attr);
			    }
			  }
		});
		c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 0;
	    c.gridx = 1;
	    c.gridy = 1;
	    pane.add(message, c);
	    
	    endButton = new JButton("Ende");
	    endButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				client.signOff();
				System.exit(0);
			}
	    	
	    });
	    endButton.setPreferredSize(new Dimension(80, 30));
	    c.gridx = 1;
	    c.gridy = 2;
	    c.fill = GridBagConstraints.EAST;
	    pane.add(endButton, c);
	    
	    
	    c.gridx = 0;
	    c.gridy = 1;
	    sendButton = new JButton("Send");
	    sendButton.setPreferredSize(new Dimension(100, 23));
	    sendButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				new Thread(new SenderThread(client, name + ": " + message.getText() + newline)).start();
				message.setText("");
			}
	    });
	    pane.add(sendButton, c);
		pack();
		
		UpdateThread up = new UpdateThread(client, this);
		new Thread(up).start();
		
		ReceiverThread rcv = new ReceiverThread(client, this);	
		new Thread(rcv).start();
	}
	
	public void updateUser(Set<User> users){
		StringBuilder sb = new StringBuilder();
		
		Iterator<User> it = users.iterator();
		while(it.hasNext()){
			User u = it.next();
			sb.append(u+"\n");
		}
		
		userlist.setText(sb.toString());
		scroll1.revalidate();
		scroll1.repaint();
		pane.revalidate();
		pane.repaint();
	}
	
	public void receive(final String s){
		SwingUtilities.invokeLater(new Runnable(){
			@Override
			public void run(){
				protocol.append(s);
				//protocol.setText(protocol.getText()+s);
				scroll2.revalidate();
				scroll2.repaint();
			}
		});
	}
	
	private class Key implements KeyListener{

		@Override
		public void keyPressed(KeyEvent e) {
			if(e.getKeyCode() == KeyEvent.VK_ENTER){
				sendButton.doClick();
			}
			
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private class ClickListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(!loggedIn){
				try {
					client.connect(host.getText(), 50000);
					loggedIn = client.signIn(username.getText());
					if(!loggedIn){
						label.setText("Login fehlgeschlagen");
					}
					else{
						name = username.getText();
						Thread t = new Thread(new Runnable(){

							@Override
							public void run() {
								pane.removeAll();
								pane.revalidate();
								pane.repaint();
								createChatGUI(pane);
							}
							
						});
						
						t.start();
					}
				} catch (UnknownHostException e1) {
					label.setText("Unbekannter Server");
				} catch(ConnectException e1){
					label.setText("Server weist Verbindung zurück.");
				} catch (IOException e1) {
					label.setText(e1.getMessage());
				}
				
			}
		}
	}
	
	private class MyWindoListener implements WindowListener {

		@Override
		public void windowActivated(WindowEvent e) {

		}

		@Override
		public void windowClosed(WindowEvent e) {

		}

		@Override
		public void windowClosing(WindowEvent e) {
			if(loggedIn){
				client.signOff();
			}
		}

		@Override
		public void windowDeactivated(WindowEvent e) {

		}

		@Override
		public void windowDeiconified(WindowEvent e) {

		}

		@Override
		public void windowIconified(WindowEvent e) {

		}

		@Override
		public void windowOpened(WindowEvent e) {

		}

	}
}