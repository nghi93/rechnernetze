package aufgabe3.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Set;
import java.util.logging.Logger;
import aufgabe3.util.*;

public class ChatClient {
	private Socket socket;
	private static final Logger log = Logger.getLogger( ChatClient.class.getName() );
	private int MAX_SIZE = 255;
	private User user;
	private Set<User> users;
	
	public static void main(String args[]){
		new Gui(new ChatClient());
	}
	
	public ChatClient(){
	}
	
	public void connect(String ip, int port) throws IOException{
		try {
			socket = new Socket(ip, port);
		} catch (UnknownHostException e) {
			throw e;
		}
		catch(ConnectException e){
			throw e;
		} 
		catch (IOException e) {
			throw e;
		}
	}
	
	public void write(String msg) throws IOException{
		log.info(msg);
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		printWriter.print(msg + "\n");
		printWriter.flush();
	}
	
	public String read() throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		StringBuilder msg = new StringBuilder();
		
		for(int i = 0; i < MAX_SIZE; i++){
			int r = bufferedReader.read();
			if(r == -1){
				return "ERROR";
			}
			char c = (char) r;
			if(c == '\n'){
				log.info(new String(msg));
				return new String(msg);
			}
			msg.append(c); 			
		}
		log.info(new String(msg));
		return new String(msg);
	}
	
	public boolean signIn(String name){
		try {
			write("NEW "+name);
			String s = read();
			if(s.startsWith("OK")){
				this.user = new UserImpl(name, socket.getInetAddress().getHostAddress(), socket.getPort());
				return true;
			}
//			else if(s.startsWith("ERROR")){
//				socket.close();
//			}
			else{
				log.warning(s);
				return false;
			}
		} catch (IOException e) {
			log.severe(e.toString());
		}
		
		return false;
	}		
	
	public User getUser(){
		return user;
	}
	
	public Set<User> getUsers(){
		return users;
	}
	
	public void updateUsers(Set<User> user){
		users = user;
	}
	
	public void signOff(){
		try {
			write("BYE");
			String s = read();
			if(s.equals("BYE")){
				socket.close();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
