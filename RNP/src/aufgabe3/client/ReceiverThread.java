package aufgabe3.client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Logger;

public class ReceiverThread implements Runnable{
	private Gui gui;
	private DatagramSocket socket;
	private final int maxMessageSize = 123;
	private final int port = 50001;
	private static final Logger log = Logger.getLogger(ReceiverThread.class.getName() );
	
	public ReceiverThread(ChatClient client, Gui gui){
		this.gui = gui;
		try {
			this.socket = new DatagramSocket(port);
		} catch (SocketException e) {
			log.severe(e.getMessage());
		}		
	}
	
	@Override
	public void run() {
		while(true){
			String msg = receive();
			// Entspricht empfangene Nachricht dem Format <Name>: <Nachricht><newline> ?
			if(msg.matches("^[a-zA-Z0-9]{1,20}:\\s.{1,100}\\n$")){
				sendToClient(msg);
			}
			else{
				log.info(msg + " verworfen.");
			}
		}		
	}
	
	public String receive(){
		byte[] receive = new byte[maxMessageSize];
		DatagramPacket dp = new DatagramPacket(receive, receive.length);
		try {
			socket.receive(dp);
			int length = dp.getLength();

			String res = new String(dp.getData(), 0, length, "UTF-8");
			log.info("Empfangen: " + res);
			return res;
		} catch (IOException e) {
			log.severe(e.getMessage());
		}		
		return "";
	}
	
	private void sendToClient(String msg){
		gui.receive(msg);
	}
}
