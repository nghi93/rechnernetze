package aufgabe3.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import aufgabe3.util.User;

public class SenderThread implements Runnable{
	private ChatClient client;
	private String messageToSend;
	private DatagramSocket socket;
	private static final Logger log = Logger.getLogger( SenderThread.class.getName() );
	
	public SenderThread(ChatClient client, String message){
		this.client = client;
		this.messageToSend = message;
		try {
			socket = new DatagramSocket();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		int sendSize = messageToSend.length();
		byte[] sendData = new byte[sendSize];       
        try {
			sendData = messageToSend.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
        // Nachricht an alle anderen Clients senden
        for(User u : client.getUsers()){
	        DatagramPacket sendPacket;
			try {
				sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(u.getHost()), u.getPort());
				log.info("Sende " + sendData + " an " + u.getHost()+":"+u.getPort());
				socket.send(sendPacket);
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} 
			catch (IOException e) {
				log.severe(e.getMessage());
			}
        }
	}
}
