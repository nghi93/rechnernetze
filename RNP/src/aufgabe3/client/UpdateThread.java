package aufgabe3.client;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import aufgabe3.util.*;

public class UpdateThread implements Runnable{

	private ChatClient client;
	private Gui gui;
	
	public UpdateThread(ChatClient client, Gui gui){
		this.client = client;
		this.gui = gui;
	}
	
	@Override
	public void run() {
		while(true){
			try {
				client.write("INFO");
				String msg = client.read();
				String[] read = msg.split(" ");	
				if(read[0].equals("LIST")){
					updateUserList(read);
				}
				else if(read[0].startsWith("ERROR")){
					client.signOff();
				}
				Thread.sleep(5000);
			} catch (IOException e) {
				return;
			}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateUserList(String[] msg){
		int numberOfUsers = Integer.valueOf(msg[1]);
		Set<User> users = new HashSet<User>();
		int startIndexOfUser = 2;
		for(int i = 1; i <= numberOfUsers; startIndexOfUser += 2, i++){
			String host = msg[startIndexOfUser];
			String name = msg[startIndexOfUser + 1];
			User u;

			u = new UserImpl(name, host, 50001);
			users.add(u);

		}
		client.updateUsers(users);
		gui.updateUser(users);
	}

}
