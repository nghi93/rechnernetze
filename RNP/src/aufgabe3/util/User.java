package aufgabe3.util;


public interface User {
	public String getName();
	public String getHost();
	public int getPort();
}
