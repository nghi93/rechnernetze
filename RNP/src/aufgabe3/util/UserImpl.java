package aufgabe3.util;


public class UserImpl implements User{
	private String name;
	private String host;
	private int port;
	
	public UserImpl(String name, String host, int port){
		this.name = name;
		this.host = host;
		this.port = port;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public String getHost() {
		return host;
	}
	
	@Override
	public int getPort() {
		return port;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o){
		if(!(o instanceof User)) return false;
		User u = (User) o;
		return this.name.equals(u.getName());
	}

	@Override
	public String toString(){
		return name;
	}
}
