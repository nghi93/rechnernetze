package aufgabe2.server;
/**
 * Repräsentiert den UpdateState nach RFC1939.
 * @author khangnghi, Heinrich
 *
 */
public class UpdateState extends AbstractState {
	POP3Server server;
	
	public UpdateState(POP3Server server){
		this.server = server;
	}

	@Override
	public String QUIT() {
		server.getUser().removeTmpMails();
		server.setState(server.getAuthorizationState());
		return POSITIVE_INDICATOR+"dewey POP3 server signing off"+CRLF;
	}
}
