package aufgabe2.server.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import aufgabe2.util.FileService;
import aufgabe2.util.Mail;
import aufgabe2.util.POP3User;

public class FileServiceTest {
	private String path;
	private FileService fileService;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		fileService = new FileService();
		this.path = fileService.getPath();
	}

	@After
	public void tearDown() throws Exception {
		String file = "test.file";
		File f = new File(path+file);
		f.delete();
		
		String file2 = "dirdoesnotexist/test.file";
		File f2 = new File(path+file2);
		f2.delete();
		new File(path+"dirdoesnotexist").delete();
		new File(path+"test.obj").delete();
		
		fileService.delete("mails/a1");
		fileService.delete("mails/a2");
		fileService.delete("mails/a3");
		fileService.delete("mails/a4");
		
		fileService.delete("mails");
		
		fileService.delete("test.obj");
		fileService.delete("user1test");
	}

	@Test
	public void testCreateFileString() {
		try {
			String file = "test.file";
			fileService.createFile(file);
			File f = new File(path+file);
			assertTrue((f.exists() && !f.isDirectory()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testCreateFileStringBoolean() {
		try {
			String file = "dirdoesnotexist/test.file";
			fileService.createFile(file, true);
			File f = new File(path+file);
			assertTrue((f.exists() && !f.isDirectory()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testCreateDir() {
		String dir = path+"dirdoesnotexist";
		assertTrue(fileService.createDir(dir));
	}

	@Test
	public void testDelete() {
		try {
			fileService.createFile("test.file");
			assertTrue(fileService.delete("test.file"));
			
			assertTrue(fileService.createDir(path+"dir"));
			assertTrue(fileService.delete("dir"));
		} catch (IOException e) {
			assertTrue(false);
		}
		
	}

	@Test
	public void testRead() {
		try {
			Mail obj2  = new Mail("12", "asa", "user1");
			fileService.write("test.obj", obj2, false);
			Mail obj = null;
			obj = (Mail)fileService.read("test.obj");	
			assertEquals(obj, obj2);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}

	@Test
	public void testWriteStringObject() {
		try {
			Mail obj2  = new Mail("12", "asa", "user1");
			fileService.write("test.obj", obj2);
			Mail obj = null;
			FileInputStream fileIn = new FileInputStream(path + "test.obj");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			obj = (Mail) in.readObject();
			in.close();
			fileIn.close();			
			assertEquals(obj, obj2);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testWriteStringObjectNotExistingDir() {
		try {
			Mail obj2  = new Mail("12", "asa", "user1");
			fileService.write("dirdoesnotexist/test.obj", obj2);
			Mail obj = null;
			FileInputStream fileIn = new FileInputStream(path + "dirdoesnotexist/test.obj");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			obj = (Mail) in.readObject();
			in.close();
			fileIn.close();			
			assertEquals(obj, obj2);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	@Test
	public void createTestMails(){
		try {
			Mail m = new Mail("a1", "gainsgsaingo\r\n", "user1");
			fileService.write("mails/a1", m, true);
			m.setUID("a2");
			fileService.write("mails/a2", m);
			m.setUID("a3");
			fileService.write("mails/a3", m);
			m.setUID("a4");
			fileService.write("mails/a4", m);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void createAndReadUser1(){
		POP3User u = new POP3User("user1","user1");
		List<Mail> res = new ArrayList<Mail>();
		List<String> mails = fileService.listFileNames("mails");
		for(String s : mails){
			Mail m = (Mail) fileService.read("mails/"+s);
			//System.out.println(m.getUID());
			res.add(m);
		}
		try {
			fileService.write("user1test", u);
			POP3User u2 = (POP3User) fileService.read("user1test");
			assertTrue(u2 != null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
