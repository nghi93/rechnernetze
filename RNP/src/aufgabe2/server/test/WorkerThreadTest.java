package aufgabe2.server.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import aufgabe2.server.WorkerThread;
import aufgabe2.util.POP3User;

public class WorkerThreadTest {
	public final String PATH = "Z:/win7/4. Semester/RN/Praktikum/RNP/src/aufgabe2/server/mailboxes//";
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetCommand() {
		WorkerThread w = new WorkerThread(null, null, null);
		String s = "LIST 1";
		String res = w.getCommand(s);
		assertEquals("LIST", res);
	}

	@Test
	public void testGetArgs() {
		WorkerThread w = new WorkerThread(null, null, null);
		String s = "LIST 1";
		String res = w.getArgs(s);
		assertEquals("1", res);
		
		s = "LIST 1 2";
		res = w.getArgs(s);
		assertEquals("1", res);		
	}
	

	public void createUserDir(){
		String user = "user1";
		File f = new File(PATH);
		f.mkdir();
		File f2 = new File(PATH + "\\" + user);
		f2.mkdir();
		File f3 = new File(PATH + "\\" + user + "\\" + "mails");
		f3.mkdir();
	}
	
	public void craeteUser(){
		String name = "user1";	
		
		POP3User user = new POP3User(name, name);

		
		// User in Datei speichern
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(PATH + name + "\\" + name + ".ser");
		    ObjectOutputStream out = new ObjectOutputStream(fileOut);
		    out.writeObject(user);
		    out.close();
		    fileOut.close();			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	public void makeUser(){
		String name = "user1";	
		
		POP3User m = null;
		try{
			FileInputStream fileIn = new FileInputStream(PATH + name + "\\" + name +".ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			m = (POP3User) in.readObject();
			in.close();
			fileIn.close();
		}
		catch(IOException i){
			
	    }
		catch(ClassNotFoundException i){

		}
		System.out.println(m.getName());
		System.out.println(m.getPassword());
		System.out.println(m);
	}
}
