package aufgabe2.server;

import aufgabe2.util.FileService;
import aufgabe2.util.POP3User;

/**
 * Repräsentiert den AuthorizationState nach RFC1939.
 * @author khangnghi, Heinrich
 *
 */
public class AuthorizationState extends AbstractState {

	POP3Server server;
	POP3User user;
	FileService fileservice;
	boolean userSuccess = false;
	
	public AuthorizationState(POP3Server server){
		this.server = server;
		this.fileservice = new FileService();
	}	
	
	@Override
	public String USER(String name) {
		getUserByFile(name);

		if(isValidMailbox(name)){
			userSuccess = true;
			return POSITIVE_INDICATOR +CRLF;
		}
		else{
			return POSITIVE_INDICATOR +CRLF;
		}
	}

	@Override
	public String PASS(String passwd) {
		if(userSuccess){
			if(isValidPassword(passwd)){
				server.setUser(user);
				server.setState(server.TRANSACTION);
				return POSITIVE_INDICATOR + " maildrop locked and ready"+CRLF;
			}
			else{
				return NEGATIVE_INDICATOR + "authorisation failed"+CRLF;
			}
		}
		else{
			return NEGATIVE_INDICATOR+ "authorisation failed"+CRLF;
		}
	}

	public void getUserByFile(String name){
		POP3User m = (POP3User) fileservice.read(name);
		if(m != null){
			this.user = m;
		}
	}
	
	public boolean isValidMailbox(String name){
		// Wenn es den User nicht gibt, existiert keine Datei von ihm und "this.user" ist null.
		if(user != null){
			return true;
		}
		else return false;
	}
	
	
	public boolean isValidPassword(String pass){
		return (user.getPassword()).equals(pass);
	}
}
