package aufgabe2.server;

import aufgabe2.util.Mail;
import aufgabe2.util.POP3User;
/**
 * Repräsentiert den TransactionState nach RFC1939.
 * @author khangnghi, Heinrich
 *
 */
public class TransactionState extends AbstractState {

	POP3Server server;

	public TransactionState(POP3Server server){
		this.server = server;
	}	

	@Override
	public String QUIT() {		
		server.setState(server.getUpdateState());
		return server.QUIT();
	}

	@Override
	public String STAT() {
		POP3User user = server.getUser();
		return POSITIVE_INDICATOR+user.numberOfMails()+" "+user.sizeOfAllMails()+CRLF;
	}

	@Override
	public String LIST(String msg) {
		POP3User user = server.getUser();
		int i = Integer.valueOf(msg);
		Mail m = user.getMailById(i);
		if(m == null){
			return NEGATIVE_INDICATOR+"no such message, only "+user.numberOfMails()+" messages in maildrop"+CRLF;
		}
		else{
			return POSITIVE_INDICATOR+" msg "+m.getSizeInOctets()+CRLF;
		}
	}

	@Override
	public String LIST() {
		POP3User user = server.getUser();
		StringBuilder sb = new StringBuilder();
		sb.append(POSITIVE_INDICATOR+user.numberOfMails()+" messages ("+user.sizeOfAllMails()+" octets)"+CRLF);
		
		for(int i = 1; i <= user.numberOfMails(); i++){
			Mail m = user.getMailById(i);
			sb.append(i+" "+m.getSizeInOctets()+CRLF);
		}
		sb.append("."+CRLF);
		return sb.toString();
	}

	@Override
	public String RETR(String msg) {
		POP3User user = server.getUser();
		StringBuilder sb = new StringBuilder();
		int i = Integer.valueOf(msg);
		Mail m = user.getMailById(i);
		sb.append(POSITIVE_INDICATOR+m.getSizeInOctets()+" octets"+CRLF);
		sb.append(m.toString());
		sb.append("."+CRLF);
		return sb.toString();
	}

	@Override
	public String DELE(String msg) {
		POP3User user = server.getUser();
		int i = Integer.valueOf(msg);
		boolean res = user.markMailAsDeletedById(i);
		
		if(res){
			return POSITIVE_INDICATOR+" message "+i+" deleted"+CRLF;
		}
		else{
			return NEGATIVE_INDICATOR+" no such message"+CRLF;
		}
	}

	@Override
	public String NOOP() {
		return POSITIVE_INDICATOR + CRLF;
	}

	@Override
	public String RSET() {
		POP3User user = server.getUser();
		user.resetDelete();
		return POSITIVE_INDICATOR;
	}

	@Override
	public String UIDL(String msg) {
		POP3User user = server.getUser();
		int i = Integer.valueOf(msg);
		Mail m = user.getMailById(i);
		
		if(m == null){
			return NEGATIVE_INDICATOR+"no such message, only "+user.numberOfMails()+" messages in maildrop"+CRLF;
		}
		else{
			return POSITIVE_INDICATOR+i+" "+m.getUID()+CRLF;
		}
	}

	@Override
	public String UIDL() {
		POP3User user = server.getUser();
		StringBuilder sb = new StringBuilder();
		sb.append(POSITIVE_INDICATOR+CRLF);
		for(int i = 1; i <= user.numberOfMails(); i++){
			sb.append(i+" "+user.getMailById(i).getUID()+CRLF);
		}
		sb.append("."+CRLF);
		
		return sb.toString();
	}


}
