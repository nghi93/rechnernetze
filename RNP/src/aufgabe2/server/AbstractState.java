package aufgabe2.server;
/**
 * Default-Implementation f�r die Commands nach RFC1939.
 * Wird von den konkreten States �berschrieben, sofern sie ein anderes Verhalten 
 * w�nschen.
 * @author Khang Nghi Lam, Heinrich Latreider
 *
 */
public abstract class AbstractState implements State{
	protected final String CRLF = "\r\n";
	protected final String POSITIVE_INDICATOR = "+OK ";
	protected final String NEGATIVE_INDICATOR = "-ERR ";
	protected final String ERR_ONLY_TRANSACTION_STATE = " may only be given in the TRANSACTION state";

	@Override
	public String USER(String name) {
		return NEGATIVE_INDICATOR+"may only be given in AUTHORIZATION state"+CRLF;
	}	
	
	@Override
	public String PASS(String string){
		return NEGATIVE_INDICATOR+"may only be given in AUTHORIZATION state immediately after a successful USER command"+CRLF;
	}
	
	@Override	
	public String QUIT(){
		return POSITIVE_INDICATOR+"dewey POP3 server signing off"+CRLF;
	}
	
	@Override
	public String STAT(){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;
	}
	
	@Override
	public String LIST(String msg){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;
	}
	
	@Override
	public String LIST(){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;
	}
	
	@Override
	public String RETR(String msg){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;
	}
	
	@Override
	public String DELE(String msg){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;		
	}
	
	@Override
	public String NOOP(){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;
	}
	
	@Override
	public String RSET(){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;
	}
	
	@Override
	public String UIDL(String msg){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;
	}
	
	@Override
	public String UIDL(){
		return NEGATIVE_INDICATOR+ERR_ONLY_TRANSACTION_STATE+CRLF;		
	}
	
	@Override
	public String GREETING(){
		return POSITIVE_INDICATOR+"POP3 server ready."+CRLF;
	}
}
