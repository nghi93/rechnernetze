package aufgabe2.server;

import aufgabe2.util.POP3User;

/**
 * 
 * @author Khang Nghi Lam, Heinrich Latreider
 * 
 * POP3Server besitzt 3 States: TRANSACTION, AUTHORIZATION, UPDATE.
 * Alle 3 States implementieren die geforderten Commands USER, PASS, QUIT, STAT, LIST, RETR, DELE, NOOP, RSET, UIDL.
 * 
 * Der Dispatcher akzeptiert nur einen Client. Der WorkerThread reicht die Eingaben hoch an den Server, der
 * die Commands ebenfalls implementiert und delegiert sie weiter an seine States.
 * Diese entscheiden dann, was zu tun ist.
 */

public class POP3Server implements State{
	State TRANSACTION = new TransactionState(this);
	State AUTHORIZATION = new AuthorizationState(this);
	State UPDATE = new UpdateState(this);
		
	State state = AUTHORIZATION;

	private DispatcherThread dispatcherThread;
	private int port;
	private POP3User user;
	

	public POP3Server(int port){
		this.port = port;
		dispatcherThread = new DispatcherThread(this.port, 1, this);
		Thread dispatcher = new Thread(dispatcherThread);
		dispatcher.start();		
	}	
	
	public static void main(String[] args) {
		new POP3Server(11000);
	}	
	
	public void setState(State state){
		this.state = state;
	}
	
	public State getTransactionState(){
		return TRANSACTION;
	}
	
	public State getAuthorizationState(){
		return AUTHORIZATION;
	}
	
	public State getUpdateState(){
		return UPDATE;
	}

	@Override
	public String USER(String name) {
		return state.USER(name);
	}

	@Override
	public String PASS(String string) {
		return state.PASS(string);
	}

	@Override
	public String QUIT() {
		return state.QUIT();
	}

	@Override
	public String STAT() {
		return state.STAT();
	}

	@Override
	public String LIST(String msg) {
		return state.LIST(msg);
	}

	@Override
	public String LIST() {
		return state.LIST();
	}

	@Override
	public String RETR(String msg) {
		return state.RETR(msg);
	}

	@Override
	public String DELE(String msg) {
		return state.DELE(msg);
	}

	@Override
	public String NOOP() {
		return state.NOOP();
	}

	@Override
	public String RSET() {
		return state.RSET();
	}

	@Override
	public String UIDL(String msg) {
		return state.UIDL(msg);
	}

	@Override
	public String UIDL() {
		return state.UIDL();
	}
	
	public String GREETING(){
		return AUTHORIZATION.GREETING();
	}
	
	public void close(){
		System.exit(0);
	}
	
	public void setUser(POP3User name){
		this.user = name;
	}
	
	public POP3User getUser(){
		return this.user;
	}
}