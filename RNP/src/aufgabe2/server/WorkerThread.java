package aufgabe2.server;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WorkerThread implements Runnable{
	private Socket socket;
	private POP3Server server;
	private final int MAX_SIZE = 255;
	private static final Logger log = Logger.getLogger( WorkerThread.class.getName() );
	private DispatcherThread dispatcher;
	private InputStreamReader reader;
	
	public WorkerThread(Socket socket, POP3Server server, DispatcherThread dispatcher){
		this.socket = socket;
		this.server = server;
		this.dispatcher = dispatcher;
		Handler handler;
		try {
			handler = new FileHandler( "server.txt" );
			log.addHandler( handler );
			handler.setLevel(Level.INFO);
		} catch (SecurityException e) {
			log.severe(e.toString());
		} catch (IOException e) {
			log.severe(e.toString());
		}
		
	}

	@Override
	public void run() {
		try{		
			reader = new InputStreamReader(socket.getInputStream());
			write(server.GREETING());
			while(true){
				try{
					String read = read();
					String command = getCommand(read);
					String getArgs = getArgs(read); 	//kann auch null sein
					
					String ret = "";
					
					if(command.equals("USER")){
						ret = server.USER(getArgs);
					}
					else if(command.equals("PASS")){
						ret = server.PASS(getArgs);
					}
					else if(command.equals("QUIT")){
						ret = server.QUIT();
						write(ret);
						socket.close();
						dispatcher.close();
						return;
					}
					else if(command.equals("STAT")){
						ret = server.STAT();
					}			
					else if(command.equals("LIST")){
						if(getArgs != null){
							ret = server.LIST(getArgs);
						}
						else{
							ret = server.LIST();
						}
					}
					else if(command.equals("RETR")){
						ret = server.RETR(getArgs);
					}
					else if(command.equals("DELE")){
						ret = server.DELE(getArgs);
					}
					else if(command.equals("NOOP")){
						ret = server.NOOP();
					}
					else if(command.equals("RSET")){
						ret = server.RSET();
					}
					else if(command.equals("UIDL")){
						if(getArgs != null){
							ret = server.UIDL(getArgs);
						}
						else{
							ret = server.UIDL();
						}
					}
					else if(command.equals("CAPA") || command.equals("AUTH")){
						ret = "-ERR\r\n";
					}
					else{
						ret = "-ERR UNKOWN COMMAND\r\n";
					}
					write(ret);		
				} catch(IOException e){
					log.severe(e.toString());
				}
			}
		} catch (IOException e) {
			log.severe(e.toString());
		}
	}
	
	public String read() throws IOException{
		StringBuilder msg = new StringBuilder();
		
		for(int i = 0; i < MAX_SIZE; i++){
			int a = reader.read();			
			if(a == -1){
				return "QUIT";
			}
			
			char c = (char) a;
			if(c=='\n' && msg.charAt(msg.length()-1)=='\r'){
				// CRLF abschneiden
				msg.deleteCharAt(msg.length()-1);
				log.info("Msg erhalten 1: " + msg);
				return new String(msg);
			}
			msg.append(c); 
		}
		log.info("Msg erhalten 2: " + msg);
		return new String(msg);
	}
	
	public void write(String msg) throws IOException{
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		log.info("Msg senden: "+msg+"\n");
		printWriter.print(msg);
		printWriter.flush();
	}
	
	public String getCommand(String s){
		String res[] = s.split(" ");
		return res[0];
	}
	
	public String getArgs(String s){
		String res[] = s.split(" ");
		
		if(res.length > 1){
			return res[1];
		}
		else{
			return null;
		}
	}
}
