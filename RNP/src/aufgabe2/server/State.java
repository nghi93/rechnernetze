package aufgabe2.server;

public interface State {
	/**
	 * Commands nach RFC1939.
	 * @see http://www.rfc-editor.org/pdfrfc/rfc1939.txt.pdf
	 */
	/**
	 * Meldet der Use
	 * @param name Login-Name
	 * @return +OK, wenn User vorhanden, -ERR, wenn User nicht vorhanden
	 */
	public abstract String USER(String name);
	public abstract String PASS(String string);
	public abstract String QUIT();
	public abstract String STAT();
	public abstract String LIST(String msg);
	public abstract String LIST();
	public abstract String RETR(String msg);
	public abstract String DELE(String msg);
	public abstract String NOOP();
	public abstract String RSET();
	public abstract String UIDL(String msg);
	public abstract String UIDL();
	/**
	 * Begr��ungstext f�r anmeldende Clients
	 */
	public abstract String GREETING();
}
