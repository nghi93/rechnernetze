package aufgabe2.client;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import aufgabe2.util.FileService;
import aufgabe2.util.Mail;
import aufgabe2.util.POP3Account;

/**
 * Ruft alle eingetragenen POP3-Konten in 30-sekuden Abst�den ab, l�scht sie 
 * von den Konten und speichert sie ab.
 *
 */
public class AbholThread implements Runnable{
	private Socket socket;
	private List<POP3Account> pop3accounts;
	private FileService fileservice;
	
	private final int SLEEPTIME = 30000; // 30 Sekunden warten
	private final String POS_REGEX = "^\\+OK.*"; 
	private final String CRLF = "\r\n";
	private static final Logger log = Logger.getLogger( AbholThread.class.getName() );
	
	public AbholThread(){
		fileservice = new FileService();
		pop3accounts = new ArrayList<POP3Account>();
		Handler handler;
		try {
			handler = new FileHandler( "client.txt" );
			log.addHandler( handler );
			handler.setLevel(Level.INFO);
		} catch (SecurityException e) {
			log.severe(e.toString());
		} catch (IOException e) {
			log.severe(e.toString());
		}		
	}
		
	@Override
	public void run(){
		while(true){	
			fetchMails();
		}
	}
	
	/**
	 * Aktualisiert die Accounts-Liste vom Abhol-Account.
	 */
	private void refreshAccountList(){
		if(fileservice.fileExists("accounts.ser")){
			String accountFile = (String)fileservice.read("accounts.ser");
			if(accountFile != null){
				pop3accounts.clear();
				String[] accounts = accountFile.split("\n");
				for(String s : accounts){
					String[] accdata = s.split(";");
					POP3Account acc = new POP3Account(accdata[0],accdata[1],accdata[2],Integer.valueOf(accdata[3]));
					pop3accounts.add(acc);
				}
			}
		}
	}
	
	/**
	 * Holt die Mails ab
	 * @post Mails liegen im Verzeichnis des Abhol-Konto
	 */
	private void fetchMails(){
		refreshAccountList();
		for(POP3Account acc : pop3accounts){
			connect(acc.getServerIP(), acc.getPort());
			try{
				if(authenticate(acc.getUsername(), acc.getPassword())){
	
					this.write("STAT");
					String msg = this.readLine();
					
					if(msg.matches(POS_REGEX)){
						String[] ret = msg.split(" ");
						int messages_to_retrieve = Integer.valueOf(ret[1]);
						log.info(messages_to_retrieve + " Mails zu empfangen");
						
						retrieveMails(acc.getUsername(), messages_to_retrieve);
					}
					Thread.sleep(SLEEPTIME);	
				}
				else{
					log.info("Authentifizierung fehlgeschlagen\nServer: "+acc.getServerIP()+":"+acc.getPort()+"User: "+acc.getUsername()+", Password: "+acc.getPassword());
				}
			} 
			catch (SocketException e){	
				log.severe(e.toString());	
			} 
			catch (IOException e){
				log.severe(e.toString());		
			} 
			catch (InterruptedException e){
				log.severe(e.toString());	
			}
		}
	}
	
	/**
	 * Authentifiziert sich bei den POP3Konten mit User und Password.
	 */
	private boolean authenticate(String user, String password) throws IOException{
		this.write("USER " + user);
		String msg = this.readLine();
		
		if(msg.matches(POS_REGEX)){
			this.write("PASS " + password);
			String msg2 = this.readLine();
			if(msg2.matches(POS_REGEX)) return true;
			else return false; 
		}
		return false;
	}
	
	/**
	 * Verbindet sich mit den Servern der POP3-Konten.
	 */
	private void connect(String ip, int port){
		try{
			log.info("Verbinde mit: " + ip + " " + port);
			socket = new Socket(ip, port);
			
			String ret = this.readLine();
			if(!ret.matches(POS_REGEX)){
				throw new SocketException("Verbindung fehlgeschlagen");
			}
		} 
		catch (UnknownHostException e){
			log.severe("Unbekannter Host!");
			System.exit(-1);
		}
		catch(ConnectException e){
			log.severe("Der Server weist die Verbindung zurueck.");
			System.exit(-1);
		} 
		catch (IOException e){
			log.severe(e.toString());
		}
	}
	
	private void write(String msg) throws IOException{
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		printWriter.print(msg + CRLF);
		log.info("Sende: " + msg);
		printWriter.flush();
	}
	
	/**
	 * Liest eine einzelne Zeile bis <CRLF>
	 */
	private String readLine() throws IOException{
		InputStreamReader reader = new InputStreamReader(socket.getInputStream());
		StringBuilder msg = new StringBuilder();

		while(true){
			int c = reader.read();
			if(c == -1) throw new SocketException();
		
			char r = (char) c;
			if(r == '\r'){
				c = (char) reader.read();
				if(c == '\n'){
					log.info("Empfange: " + msg.toString());
					return msg.toString();
				}		
			}
			msg.append(r);
		}		
	}
	
	/**
	 * Liest Multi-Line R�ckgaben der Server bis <CRLF>.<CLRF>
	 */
	public String readMultiLine() throws IOException{
		InputStreamReader reader = new InputStreamReader(socket.getInputStream());
		StringBuilder msg = new StringBuilder();
		
		while(true){
			int c = reader.read();
			if(c == -1) throw new SocketException();
			char r = (char) c;
			msg.append(r);
			if(msg.toString().contains(CRLF + "." + CRLF)){
				log.info("Empfange: " + msg.toString());
				return msg.toString();
			}			
		}
	}
	
	/**
	 * Holt die Mails von den POP3-Konten ab, speichert sie und l�scht sie von den POP3-Konten.
	 */
	public void retrieveMails(String user, int count) throws IOException{
		for(int i = 1; i <= count; i++){
			write("RETR " + i);
			String mail = this.readMultiLine();
			
			if(mail.startsWith("+OK")){
				// 1. Zeile abschneiden
				mail = mail.substring(mail.indexOf('\n')+1);
				// Letzte Zeile abschneiden
				int charsAtLastLine = 3;
				mail = mail.substring(0, mail.length() - charsAtLastLine);
				
				// Timestamp
				// http://stackoverflow.com/questions/1459656/how-to-get-the-current-time-in-yyyy-mm-dd-hhmisec-millisecond-format-in-java
				SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			    Date now = new Date();
			    String strDate = sdfDate.format(now);
			    
				Mail m = new Mail(strDate, mail, user);
				saveMail(user, m);
			}
			else{
				log.warning("Mail existiert nicht");
			}
		}

		for(int i = 1; i <= count; i++){
			write("DELE "+ i);
		}
		write("QUIT");
	}
	
	/**
	 * Speichert eine Mail ab.
	 */
	private void saveMail(String user, Mail m){
		try {
			fileservice.write("mails/" + user + m.getUID(), m, true);
		} catch (IOException e) {
			log.severe(e.toString());
		}
	    
	}
	
}