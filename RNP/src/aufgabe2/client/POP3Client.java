package aufgabe2.client;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Logger;
import aufgabe2.util.FileService;
import aufgabe2.util.POP3Account;
import aufgabe2.util.POP3User;

/**
 * Client holt f�r einen Abhol-Account die Mails von POP3-Konten ab.
 * @author khangnghi, Heinrich Latreider
 *
 */

public class POP3Client{
	private static Scanner scanner;
	private FileService fileservice;
	private static final Logger log = Logger.getLogger( POP3Client.class.getName() );

	public POP3Client(){
		fileservice = new FileService();
		setUp();
	}
	
	private void setUp(){
		fileservice.delete("accounts.ser");
	}
	
	public static void main(String[] args){
		POP3Client client = new POP3Client();
		scanner = new Scanner(System.in);
		
		POP3User user = new POP3User("user1", "user1");
    	
    	AbholThread abhol = new AbholThread();
    	Thread t = new Thread(abhol, "Abholthread");
    	t.start();
    	log.info("Abholthread gestartet");
    	
	    while(scanner.hasNext()){
	        String s1 = scanner.nextLine();
	        
	        String[] cmd = s1.split(" ");
	        if(cmd[0].equals("create") && cmd.length == 5){
	        	int port = Integer.valueOf(cmd[4]);
	        	client.addPOP3Account(cmd[1], cmd[2], cmd[3], port, user);
	        }
	        else{
	        	log.info("Ungueltiger Befehl");
	        }     
	    }			
	    scanner.close();		
	}
	
	/**
	 * F�gt ein neues POP3-Konto zum Abhol-Account hinzu.
	 * Dieses wird alle 30 Sekunden durch den Abholthread dann abgefragt.
	 */
	public void addPOP3Account(String name, String pass, String ip, int port, POP3User user){
		
		POP3Account account = new POP3Account(name, pass, ip, port);
		// accounts.ser existiert nicht
		if(fileservice.read("accounts.ser") != null){
			String account2 = (String) fileservice.read("accounts.ser");
			try {
				fileservice.write("accounts.ser", account2+account.toString());
			} catch (IOException e) {
				log.severe(e.toString());
			}
		}
		else{
			try {
				fileservice.write("accounts.ser", account.toString());
			} catch (IOException e) {
				log.severe(e.toString());
			}
		}
		
		
	}
}
