package aufgabe2.util;

/**
 * Repräsentiert die POP3-Konten, von denen die Mails abgeholt werden.
 * @author khangnghi, Heinrich
 *
 */
public class POP3Account implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private String username;
	private String password;
	private String serverIP;
	private int port;
	
	public POP3Account(String accname, String pass, String ip, int port){
		this.username = accname;
		this.password = pass;
		this.serverIP = ip;
		this.port = port;
	}
	
	public String getUsername(){
		return username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getServerIP(){
		return serverIP;
	}
	
	public int getPort(){
		return port;
	}
	
	public String toString(){
		return username+";"+password+";"+serverIP+";"+port+";\n";
	}
}
