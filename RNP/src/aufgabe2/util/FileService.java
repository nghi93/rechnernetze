package aufgabe2.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Regelt Zugriff auf das Datei-System.
 * @author khangnghi
 *
 */
public class FileService {
	//private final String PATH = "Z:/win7/4. Semester/RN/Praktikum/RNP/src/aufgabe2/server/mailboxes/";
	//private final String PATH = "F:/Uni/4. Semester/RN/RNP/Praktikum/RNP/src/aufgabe2/server/mailboxes/user1/";
	//private final String PATH = "D:/Users/Nghi/Uni/4. Semester/RN/Praktikum/RNP/src/aufgabe2/server/mailboxes/user1/";
	private final String PATH = "/home/stud11/abk609/win7/4.Semester/rn/Aufgabe2/RNP/Praktikum/RNP/src/aufgabe2/server/mailboxes/user1/";
	public String getPath(){
		return PATH;
	}
	
	public void createFile(String file) throws IOException{
		createFile(file, false);
	}
	
	/**
	 * @param makeDir Sollen die Verziechnisse erstellt werden, wenn es sie nicht gibt?
	 */
	public boolean createFile(String file, boolean makeDir) throws IOException{
		File f = new File(PATH+file);	
		
		try{
			return f.createNewFile();
		}
		catch(IOException e){
			if(makeDir){
				createDir(f.getParentFile().toString());
				return f.createNewFile();
			}
			else{
				throw new IOException(e.getMessage());
			}
		}
	}
	
	public boolean createDir(String dir){
		File f = new File(dir);
		return f.mkdirs();
	}
	
	public boolean delete(String file){
		File f = new File(PATH+file);
		return f.delete();
	}
	
	public Object read(String file){
		Object obj = null;
		FileInputStream fileIn;
		try {
			fileIn = new FileInputStream(PATH + file);
			ObjectInputStream in = new ObjectInputStream(fileIn);
			obj = (Object)in.readObject();
			in.close();
			fileIn.close();
			return obj;
		} catch (FileNotFoundException e) {
			return null;
		} catch (ClassNotFoundException e) {
			return null;
		} catch (IOException e) {
			return null;
		} 	
		
	}
	
	public void write(String file, Object obj) throws IOException{
		write(file, obj, false);
	}
	
	/**
	 * @param makeDir Sollen die Verzeichnisse erstellt werden, wenn die nicht existieren?
	 */
	public void write(String file, Object obj, boolean makeDir) throws IOException{
		try {
			FileOutputStream fileOut;
			fileOut = new FileOutputStream(PATH + file);
		    ObjectOutputStream out = new ObjectOutputStream(fileOut);
		    out.writeObject(obj);
		    out.close();
		    fileOut.close();	
		} catch (FileNotFoundException e) {
			if(makeDir){
				System.out.println(PATH+new File(file).getParentFile());
				createDir(PATH+new File(file).getParentFile());
				write(file, obj, makeDir);
			}
			else{
				throw new FileNotFoundException(e.getMessage());
			}
		} catch (IOException e) {
			throw new IOException(e.getMessage());
		}
	}
	
	public boolean fileExists(String file){
		File f = new File(PATH + file);
		return f.exists();
	}
	
	public List<String> listFileNames(String dir){
		if(!new File(PATH+dir).exists()){
			return new ArrayList<String>();
		}
		
		List<String> res = new ArrayList<String>();
		File file[] = new File(PATH + dir).listFiles();
		for(File f : file){
			res.add(f.getName());
		}
		return res;
		
	}
}
