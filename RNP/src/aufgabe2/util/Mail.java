package aufgabe2.util;
import java.io.IOException;
import java.io.Serializable;


public class Mail implements Serializable{
	private static final long serialVersionUID = 3682353673105527994L;
	private String UID;
	private String contents;
	private String mailbox;
	private transient FileService fileService;
	private transient boolean markedAsDeleted = false;
	
	public Mail(String UID, String contents, String path){
		this.UID = UID;
		this.contents =  contents;
		this.mailbox = path;
	}
	
	public void setMailbox(String path){
		this.mailbox = path;
	}
	
	public void setContents(String s){
		contents = s;
	}
			
	public String getContents(){
		return contents;
	}
	
	public int getSizeInOctets(){
		return contents.length();
	}
	
	public String toString(){
		return contents;
	}
	
	public void setUID(String id){
		this.UID = id;
	}
	
	public String getUID(){
		return UID;
	}
	
	public String getPath(){
		return mailbox;
	}
	
	public void delete(){
		fileService.delete("mails/"+ this.mailbox + this.UID);
	}
	
	public boolean markDelete(boolean b){
		boolean tmp = markedAsDeleted;
		markedAsDeleted = b;
		return !tmp;
	}
	
	public boolean isMarkedAsDeleted(){
		return markedAsDeleted;
	}
	
	@Override
	public boolean equals(Object o){
		if(!(o instanceof Mail)){
			return false;
		}
		Mail m = (Mail) o;
		return this.getUID().equals(m.getUID()) && this.getContents().equals(m.getContents());
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		fileService = new FileService();
		markedAsDeleted = false;
	}	
}
