package aufgabe2.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Repr�sentiert den Abhol-Account.
 * @author khangnghi, Heinrich
 *
 */
public class POP3User implements Serializable{

	private static final long serialVersionUID = 1L;
	private String mailbox;
	private String password;
	private transient List<Mail> mails;
	private transient FileService fileService;
	
	public POP3User(String name, String password){
		this.mailbox = name;
		this.password = password;
		mails = new ArrayList<Mail>();
		fileService = new FileService();
	}
	
	public String getPassword(){
		return password;
	}
	
	public String getName(){
		return mailbox;
	}
	
	public void setMail(List<Mail> l){
		this.mails = l;
	}
	
	public int numberOfMails(){
		return mails.size();
	}
	
	public int sizeOfAllMails(){
		int res = 0;
		for(Mail m : mails){
			res += m.getSizeInOctets();
		}
		
		return res;
	}
	
	/**
	 * Gibt alle Mails eines Users zur�ck
	 * @param user
	 * @return Leere Liste, wenn keine Mails da sind
	 * @return Liste mit Emails, wenn welche gefunden wurden.
	 */	
	public List<Mail> getMail(){
		List<Mail> res = new ArrayList<Mail>();
		List<String> mails = fileService.listFileNames("mails");
		for(String s : mails){
			System.out.println(s);
			Mail m = (Mail) fileService.read("mails/"+s);
			res.add(m);
		}
		return res;
	}
	
	/**
	 * Liefert die Mail nach ID zur�ck.
	 * 
	 * @return die Mail oder null, wenn sie nicht existiert.
	 */
	public Mail getMailById(int i){
		if(mails.size() < i || i < 1){
			return null;
		}	
		return mails.get(i-1);
	}
	
	public boolean markMailAsDeletedById(int i){
		if(i <= mails.size() && i > 0){
			return mails.get(i-1).markDelete(true);
		}	
		return false;
	}
	
	public void resetDelete(){
		for(int i = 1; i <= numberOfMails(); i++){
			getMailById(i).markDelete(false);
		}
	}
	
	public void removeTmpMails(){
		List<Mail> tmp = new ArrayList<Mail>();
		for(int i = 1; i <= numberOfMails(); i++){
			if(getMailById(i).isMarkedAsDeleted()){
				Mail m = getMailById(i);
				tmp.add(m);
				m.delete();
			}			
		}
		mails.removeAll(tmp);
	}
	
	public String toString(){
		return "POP3User[name: "+getName()+", pass: "+getPassword()+"]";
	}
	
	private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
		in.defaultReadObject();
		fileService = new FileService();
		mails = getMail();
	}
	
}
