package aufgabe1.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

public class TCPClient {
	private Socket socket;
	private static Scanner scanner;
	
	public static void main(String args[]){
		String ip;
		int port;
		String msg = "Eingabeformat: IP Port oder IP:Port";
		
		
		if(args.length < 1){
			throw new IllegalArgumentException(msg);
		}
		
		if(args.length == 1){
			String res[] = args[0].split(":");
			if(res.length < 2){
				throw new IllegalArgumentException(msg);
			}
			ip = res[0];
			port = Integer.valueOf(res[1]);
		}
		else{
			ip = args[0];
			port = Integer.valueOf(args[1]);
		}
		
		TCPClient client = new TCPClient(ip, port);
		
		try {
			scanner = new Scanner(System.in);
		    while (scanner.hasNext()) {
		        String s1 = scanner.nextLine();
		        client.write(s1);
		        String msg2 = client.read();
		        System.out.println(msg2);
		        if(msg2.equals("BYE")){
		        	System.exit(0);
		        }
		    }			
		    scanner.close();
			
		} 
		catch(SocketException e){
			System.out.println("Verbindung zum Server verloren");
		}		
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public TCPClient(String ip, int port){
		connect(ip, port);
	}
	
	public void connect(String ip, int port){
		try {
			socket = new Socket(ip, port);
		} catch (UnknownHostException e) {
			System.out.println("Unbekannter Host!");
			System.exit(-1);
		}
		catch(ConnectException e){
			System.out.println("Der Server weist die Verbindung zurueck.");
			System.exit(-1);
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void write(String msg) throws IOException{
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		printWriter.print(msg+"\n");
		printWriter.flush();
	}
	
	public String read() throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));

		StringBuilder msg = new StringBuilder();
		
		for(int i = 0; i < 255; i++){
			char c = (char)bufferedReader.read();
			

			if(c=='\n'){
				return new String(msg);
			}
			msg.append(c); 			
		}
		
		return new String(msg);
	}
}
