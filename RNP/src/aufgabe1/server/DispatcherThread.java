package aufgabe1.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Semaphore;

public class DispatcherThread implements Runnable {
	private ServerSocket serverSocket;
	private int port;
	private boolean accept = true;
	private int maxNumberOfThreads;
	private int numberOfThreads;
	private TCPServer server;
	private Semaphore sema;
	
	public DispatcherThread(int port, int maxNumberOfThreads, TCPServer server){
		this.port = port;
		try {
			serverSocket = new ServerSocket(port);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		this.maxNumberOfThreads = maxNumberOfThreads;
		sema = new Semaphore(maxNumberOfThreads);
		this.numberOfThreads = 0;
		this.server = server;
	}
	
	@Override
	public void run() {
		try {		
			Socket socket;
			while(accept){
				sema.acquire();
				socket = serverSocket.accept();
				System.out.println("Dispatcher: Anfrage bekommen");
				WorkerThread worker = new WorkerThread(socket, this);
				Thread t1 = new Thread(worker, "Worker");
				t1.start();	
			}
		} 	
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch(InterruptedException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	synchronized void close(){
		sema.release();
		if(accept == false && numberOfThreads == 0){
			try {
				serverSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			server.close();
			System.out.println("Beende Dispatcher.");
			return;
		}		
	}
	
	public void shutdown(){
		accept = false;	
	}
	
	public boolean checkPassword(String password){
		return server.checkPassword(password);
	}
}
