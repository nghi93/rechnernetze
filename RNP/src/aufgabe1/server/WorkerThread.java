package aufgabe1.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

public class WorkerThread implements Runnable{
	private Socket socket;
	private String command;
	private DispatcherThread dispatcher;
	private final int MAX_SIZE = 255;
	
	public WorkerThread(Socket socket, DispatcherThread dispatcher){
		this.socket = socket;
		this.command = "";
		this.dispatcher = dispatcher;
	}

	@Override
	public void run() {
		System.out.println("Worker: Starte WorkerThread");
		String s = "";
		try {
			while(true){
				s = read();
				System.out.println("Worker: Nachricht empfangen: "+s);	
				s = translate(s);
				write(s);
				System.out.println("Worker: Nachricht gesendet: "+s);	
				
				if(command.equals("SHUTDOWN")){
					dispatcher.shutdown();
				}
				if(command.equals("BYE")){
					try {
						dispatcher.close();
						System.out.println("Worker: Beende Thread");
						socket.close();
						System.out.println("Worker: Verbindung geschlossen");
						return;				
					} catch (IOException e) {
						e.printStackTrace();
					}
				}				
			}
		} 
		catch(SocketException e){
			System.out.println("Worker: Client disconnected");
			dispatcher.close();
			System.out.println("Worker: Beende Thread");
			try {
				socket.close();
			} catch (IOException e1) {
			}
			System.out.println("Worker: Verbindung geschlossen");
		}
		catch (IOException e) {
			System.out.println("Ein unerwarteter Fehler ist aufgetreten.");
		}
	}
	
	public String read() throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		StringBuilder msg = new StringBuilder();
		
		for(int i = 0; i < MAX_SIZE; i++){
			char c = (char)bufferedReader.read();
			

			if(c=='\n'){
				return new String(msg);
			}
			msg.append(c); 			
		}
		
		return new String(msg);
	}
	
	public void write(String msg) throws IOException{
		PrintWriter printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
		printWriter.print(msg+"\n");
		printWriter.flush();
	}
		
	
	public String translate(String s){
		String split[] = s.split(" ");
		command = split[0];

		if(command.equals("UPPERCASE")){
			return "OK "+split[1].toUpperCase();
		}
		else if (command.equals("LOWERCASE")){
			return "OK "+split[1].toLowerCase();
		}
		else if(command.equals("REVERSE")){
			return "OK "+new StringBuilder(split[1]).reverse().toString();
		}
		else if(command.equals("BYE")){
			return "BYE";
		}
		else if(command.equals("SHUTDOWN")){
			
			if(split.length > 1 && dispatcher.checkPassword(split[1])){
				return "OK";
			}
			else{
				return "ERROR Falsches Passwort";
			}
		}
		else{
			return "ERROR ungueltiger Befehl";
		}
	}
}
