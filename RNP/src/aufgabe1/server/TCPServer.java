package aufgabe1.server;

import java.util.Scanner;

public class TCPServer {
	private DispatcherThread dispatcherThread;
	private static int port;
	private String password;
	
	public static void main(String args[]){
		int maxNumberOfThreads = 10;
		String password ="123b";
		
		for(int i = 0; i < args.length; i++){
			String arg = args[i];
			if(arg.equals("-port") || arg.equals("-p")){
				port = Integer.valueOf(args[i+1]);
				i++;
			}
			else if(arg.equals("-password") || arg.equals("-pw")){
				password = args[i+1];
				i++;
			}
			else if(arg.equals("-threads") || arg.equals("-t")){
				maxNumberOfThreads = Integer.valueOf(args[i+1]);
				i++;
			}
			else{
				System.out.println("Unbekannter Befehl!");
				System.exit(-1);
			}
		}
		
		TCPServer server = new TCPServer();
		server.setUp(port, maxNumberOfThreads, password);	
		
		
		Scanner scanner = new Scanner(System.in);
	    while (scanner.hasNext()) {
	        String s1 = scanner.nextLine();
	        if(s1.equals("exit")){
	        	scanner.close();
	        	System.exit(-1);
	        }
	        else{
	        	System.out.println("Unbekannter Befehl!");
	        }
	    }		

		

	}
	
	public TCPServer(){
		
	}
	
	public void setUp(int port, int maxNumberOfThreads, String password){	
		this.password = password;
		dispatcherThread = new DispatcherThread(port, maxNumberOfThreads, this);
		Thread dispatcher = new Thread(dispatcherThread);
		dispatcher.start();
	}
	
	public void close(){
		System.out.println("Beende Serveranwendung.");
		System.exit(0);
	}
	
	public boolean checkPassword(String password){
		return password.equals(this.password);
	}
}
